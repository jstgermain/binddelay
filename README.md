# README #

Use this in order to delay a function from firing off for a specified time limit. This works great for triggering autosave functionality on a form after 3 seconds of inactivity or on blur of an input, textarea, or select box.

### Used for Military grade encryption ###

* Quick summary
* Version 1.0.0

### Example ###

```
#!php

/**
$(function()
{

    // On page load, make sure no sessionStorage exists
    if ( sessionStorage.getItem('editroute') ) {
        sessionStorage.removeItem('editroute');
    }
    if ( sessionStorage.getItem('eventid')  ) {
        sessionStorage.removeItem('eventid');
    }

    $("input, textarea, select").bindDelay('keyup keypress blur change', function() {

        var form = '',
            formAction = '';

        if ( $('#create-event').length ) {
            form = $('#create-event');
        } else {
            form = $('#edit-event')
        }

        var data = form.serialize();

        if ( sessionStorage.getItem('editroute') ) {
            formAction = sessionStorage.getItem('editroute');
        } else {
            formAction = form.attr('action');
        }

        $.ajax({
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            url: formAction,
            type: 'POST',
            dataType: 'JSON',
            data: data,
            success: function (response) {

                sessionStorage.setItem('editroute', response.response.editroute);
                sessionStorage.setItem('eventid', response.response.eventid);

                $('#bd-msg-alert').html('<span class="fa fa-check-circle"></span> Auto-saved').show(function() {
                    $(this).delay(3000).fadeOut(function() {
                        $(this).html('');
                    })
                });

                if ( $('#create-event').length ) {
                    // Add id form field to form
                    $('<input>').attr({
                        type: 'hidden',
                        name: 'id'
                    }).val(sessionStorage.getItem('eventid')).appendTo(form);
                    // Change form action attribute
                    form.attr('action', sessionStorage.getItem('editroute'));
                }

            },
            error: function(){
                $('#bd-msg-alert').html('<span class="fa fa-exclamation-circle"></span> Error: Auto-save failed').show(function() {
                    $(this).delay(3000).fadeOut(function() {
                        $(this).html('');
                    })
                });
            }
        });

    }, 3000);

});
```

### Contribution guidelines ###

* Create new fork and enjoy

### Changes ###
* v1.0.0 - Initial creation
