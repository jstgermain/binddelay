/**
 * jQuery Plugin Bind Delay
 *
 * @author Justin St. Germain (justin[at]ibrightdev[dot]com)
 * Licensed under the MIT License
 *
 * Usage:
 * $("ELEMENT").bindDelay('EVENT', function() {
 *     // If the timer reaches set time, do something
 *     alert('Timer ran out');
 * }, 3000);
 *
 */

(function($) {

	$.fn.bindDelay = function( eventType, eventData, handler, timer ) {
		if ( $.isFunction(eventData) ) {
			timer = handler;
			handler = eventData;
		}
		timer = (typeof timer === "number") ? timer : 2000;
		var timeouts;
		$(this).bind(eventType, function(event) {
			var that = this;
			clearTimeout(timeouts);
			timeouts = setTimeout(function() {
				handler.call(that, event);
			}, timer);
		});
	};

})(jQuery);

$(document).ready(function() {

	// This is included so you can send growl style messages if needed
	$('body').append('<div id="bd-msg-alert" />').find('#bd-msg-alert').css({
		'bottom': '50px',
		'right': '10px',
		'display': 'none',
		'background': 'rgba(0,0,0,0.8)',
		'border': '2px solid #000000',
		'width': '300px',
		'-webkit-box-sizing': 'border-box',
		'-moz-box-sizing': 'border-box',
		'box-sizing': 'border-box',
		'padding': '10px',
		'color': '#FFFFFF',
		'position': 'fixed',
		'z-index': '999',
		'-webkit-border-radius': '5px',
		'-moz-border-radius': '5px',
		'border-radius': '5px',
		'font-family': 'Lucida Sans Unicode, Lucida Grande, sans-serif',
		'font-size': '25px'
	});

});